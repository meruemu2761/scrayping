import time 
import traceback
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import chromedriver_binary
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select
import pandas as pd
from bs4 import BeautifulSoup
　

############################################
#＃関数を作成
############################################
#URLからHTMLを抽出する関数
def get_html(url):
    driver.get(url)
    html = driver.page_source
    soup = BeautifulSoup(html, 'html.parser')
    return soup


############################################
#＃ログイン
############################################
options = Options()

driver = webdriver.Chrome(options=options)
driver.get("https://unistyleinc.com/")

driver.find_element_by_id("LoginLink").click()

driver.find_element_by_name("user[email]").send_keys("meruemu2761@gmail.com")
driver.find_element_by_name("user[password]").send_keys("asd28236513")
driver.find_element_by_name("commit").click()

time.sleep(0.1)


############################################
#＃カテゴリーを取得
############################################
main_soup = get_html("https://unistyleinc.com/categories")

#・データフレームで列を作成
columns = ["category_name","URL"]
df = pd.DataFrame(columns=columns)

#・for文でtagsを回す
tags = main_soup.find_all("a",{"class":"es_category_item v-h-center"})

#tagから業界カテゴリー名を取得
#tagからurlを取得
for tag in tags:
    es_category_name = tag.text
    es_category_url = str("https://unistyleinc.com" + tag.get("href"))
    #　・pandasのSeriesに記事名と記事URLを代入
    se = pd.Series([es_category_name, es_category_url], columns)
    
    #　・データフレームに追加
    df = df.append(se, columns)

print(df)

select_category = int(input('スクレイピング対象のカテゴリー(数字)を指定してください。'))


if select_category not in df:
    print('正しいカテゴリーを入力してください。')
    exit( )

FILE_NAME = str(input('ファイル名を指定してください。（.csv）'))

if '.csv' not in FILE_NAME:
    print('正しくファイル名を入力してください。')
    exit( )
else:
    print('----------------------------------------')
    print('----------------------------------------')
    print('スクレイピングを実行します。')
    print('対象カテゴリー：{}'.format(select_category))
    print('出力ファイル名：{}'.format(FILE_NAME))
    print('----------------------------------------')
    print('----------------------------------------')


############################################
##各業界ページから各企業ページのURLを取得
############################################

#企業カテゴリーのリストを作成
company_url = df.loc[:,"URL"]

#各カテゴリーのURLで再帰

company_html = get_html(company_url[select_category])
company_tags = company_html.find_all("div",{"class":"relation_companies"})

company_columns = ["company_name","company_URL"]
company_df = pd.DataFrame(columns=company_columns)
    
#企業名、URLを取得
for company_tag in company_tags:
    relation_companie_name = company_tag.find("p").text
   
    relation_companie_url = "https://unistyleinc.com" + company_tag.find("a").get("href")
    company_se = pd.Series([relation_companie_name,relation_companie_url],company_columns)
    company_df = company_df.append(company_se,company_columns)
    
    #企業リストの作成
    report_url = company_df.loc[:,"company_URL"]

    #各企業のURLで再帰
    for k in report_url:
        eslist_html = get_html(k)
        eslist_tags = eslist_html.find_all("a")
        eslist_columns = ["escategory","eslist_url"]
        eslist_df = pd.DataFrame(columns=eslist_columns)
            
        for eslist_tag in eslist_tags:

            #空欄の場合はとばす
            try:
                eslist_title = eslist_tag.find("p",{"class":"research_common_title"}).text
                eslist_url = "https://unistyleinc.com" + eslist_tag.get("href")
            except:
                eslist_title = "None"
                eslist_url = "None"

            if eslist_title == "None" and eslist_url == "None":
                continue
            else:
                eslist_se = pd.Series([eslist_title,eslist_url],eslist_columns)
                eslist_df = eslist_df.append(eslist_se,eslist_columns)
            time.sleep(0.1)
            sheet_url = eslist_df.loc[:,"eslist_url"]

                
             ##ESの種類で再帰
            for l in sheet_url:
                sheet_html = get_html(l)
                sheet_tags = sheet_html.find_all("div",{"class":"es_container"})
                    
                sheet_columns = ["sheet_title","sheet_URL"]
                sheet_df = pd.DataFrame(columns=sheet_columns)
                    
                for sheet_tag in sheet_tags:
                    sheet_title = sheet_tag.find("h3").text
                    sheet_url = "https://unistyleinc.com" + sheet_tag.find("a").get("href")
                    sheet_se = pd.Series([sheet_title,sheet_url],sheet_columns)
                    sheet_df = sheet_df.append(sheet_se,sheet_columns)
                    time.sleep(1)
                    es_url = sheet_df.loc[:,"sheet_URL"]
 
                    for m in es_url[:5]:
                        driver.execute_script("window.open()") #make new tab
                        driver.switch_to.window(driver.window_handles[1]) #switch new tab
                        es_html = get_html(m)
                        es_tags = es_html.find("div",{"class":"common_width90_wrap marginb10"})
                        es_columns = ["company","sex","contents"]
                        es_df = pd.DataFrame(columns=es_columns)
                        
                        company = es_html.find("a",{"class":"tag"}).text
                        print(company)
                        sex = es_tags.find_all("span")
                        print(sex[1].text)
                        contents = es_html.find("div",{"class":"esdetail_main"}).text
                        print(contents)
                        es_se = pd.Series([company,sex,contents],es_columns)
                        es_df = es_df.append(es_se,es_columns)
                        driver.close()
                        driver.switch_to.window(driver.window_handles[0])
                        time.sleep(0.1)
    
#ファイル作成、CSV作成 
eslist_df.to_csv(FILE_NAME,encoding="utf-8-sig")
